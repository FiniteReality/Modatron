using System;
using System.Text;

namespace Modatron.Extensions
{
	public static class TimeSpanExtensions
	{
		public static string ToPrettyFormat(this TimeSpan span) {
			if (span == TimeSpan.Zero) return "0 minutes";

			var sb = new StringBuilder();
			if (span.Days > 0)
				sb.AppendFormat("{0} day{1}", span.Days, span.Days > 1 ? "s" : string.Empty);
			if (span.Hours > 0)
				sb.AppendFormat("{0}{1} hour{2}", (span.Minutes == 0 ? " and " : (span.Days > 0 ? ", " : string.Empty)), span.Hours, span.Hours > 1 ? "s" : string.Empty);
			if (span.Minutes > 0)
				sb.AppendFormat("{0}{1} minute{2}", (span.Seconds == 0 ? " and " : (span.Hours > 0 ? ", " : string.Empty)), span.Minutes, span.Minutes > 1 ? "s" : string.Empty);
			if (span.Seconds > 0)
				sb.AppendFormat("{0}{1} second{2}", ((int)span.TotalMinutes > 0 ? " and " : ""), span.Seconds, span.Seconds > 1 ? "s" : string.Empty);
			return sb.ToString();
		}
	}
}