using System;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

using Discord;
using Discord.Commands;

namespace Modatron.Readers
{
	class TimeSpanReader : TypeReader
	{
		private static Regex format = new Regex(@"(?:([\d\.]+)([YyDdHhMmSs])){1,}");
		public override Task<TypeReaderResult> Read(IUserMessage context, string input)
		{
			if (format.IsMatch(input))
			{
				var match = format.Match(input);
				TimeSpan value = TimeSpan.Zero;

				CaptureCollection numbers = match.Groups[1].Captures;
				CaptureCollection groups = match.Groups[2].Captures;

				for(int i = 0; i < numbers.Count; i++)
				{
					double groupValue = double.Parse(numbers[i].Value);
					var groupName = groups[i].Value.ToLowerInvariant();
					switch(groupName)
					{
						case "y":
							value += TimeSpan.FromDays(365.25 * groupValue);
							break;
						case "d":
							value += TimeSpan.FromDays(groupValue);
							break;
						case "h":
							value += TimeSpan.FromHours(groupValue);
							break;
						case "m":
							value += TimeSpan.FromMinutes(groupValue);
							break;
						case "s":
							value += TimeSpan.FromSeconds(groupValue);
							break;
						default:
							return Task.FromResult(TypeReaderResult.FromError(CommandError.ParseFailed, $"Unknown grouping {groupName}"));
					}
				}

				return Task.FromResult(TypeReaderResult.FromSuccess(value));
			}
			else
			{
				TimeSpan span;
				if (TimeSpan.TryParse(input, out span))
					return Task.FromResult(TypeReaderResult.FromSuccess(span));
				else
					return Task.FromResult(TypeReaderResult.FromError(CommandError.ParseFailed, "Invalid time span"));
			}
		}
	}
}