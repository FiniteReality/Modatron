using System;

namespace Modatron.Attributes
{
	[AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false)]
	public class HiddenParameterAttribute : Attribute
	{

	}
}