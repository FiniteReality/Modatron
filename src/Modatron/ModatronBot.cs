using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Collections.Concurrent;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Discord.Addons.AspNetDI;

using Modatron.Models;
using Modatron.Readers;
using Modatron.Services;

using Newtonsoft.Json;

namespace Modatron
{
	public class ModatronBot
	{
		private DiscordSocketClient client;
		private CommandService commands;
		private IServiceProvider services;
		private ISelfUser self;
		private ILogger generalLogger;
		private ILogger commandLogger;

		private DateTime startTime;

		private int ShardId;
		private int TotalShards;

		public ModatronBot()
		{
			var builder = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json");

			Configuration = builder.Build();

			client = new DiscordSocketClient(new DiscordSocketConfig
			{
				LogLevel = LogSeverity.Verbose
			});

			commands = new CommandService();
			commands.AddTypeReader<TimeSpan>(new TimeSpanReader());
		}

		public IConfigurationRoot Configuration { get; set; }

		public TimeSpan Uptime()
		{
			return (DateTime.UtcNow - startTime);
		}

		private async Task HandleMessage(IMessage _message)
		{
			if (_message.Author.Id == self.Id)
				return;
			
			if (_message is IUserMessage)
			{
				var message = _message as IUserMessage;

				int argPos = 0;
				if (message.HasMentionPrefix(self, ref argPos) || message.HasCharPrefix('.', ref argPos))
				{
					var result = await commands.Execute(message as IUserMessage, argPos);

					if (!result.IsSuccess)
					{
						if (result is ExecuteResult)
						{
							var exception = ((ExecuteResult)result).Exception;
							commandLogger.LogWarning(exception.ToString());
							
							if (exception is CommandException)
								await _message.Channel.SendMessageAsync(exception.Message);
							else
							{
								var errorMessage = await _message.Channel.SendMessageAsync($"Runtime error while executing command: {exception.Message}");
								await Task.Delay(5000);
								await errorMessage.DeleteAsync();
							}
						}
						else if (result is PreconditionResult)
						{
							var errorMessage = await _message.Channel.SendMessageAsync(result.ErrorReason);
								await Task.Delay(5000);
								await errorMessage.DeleteAsync();
						}
						else if (result is ParseResult)
						{
							if (result.Error != CommandError.UnknownCommand)
							{
								var errorMessage = await _message.Channel.SendMessageAsync($"Parse error while executing command: {result.ErrorReason}");
								await Task.Delay(5000);
								await errorMessage.DeleteAsync();
							}
						}
					}
				}
			}
		}

		private Task HandleBanTicking(int oldLatency, int newLatency)
		{
			if (client.ConnectionState == ConnectionState.Connected)
				return services.GetRequiredService<PunishmentService>().Tick();
			return Task.CompletedTask;
		}

		private ConcurrentDictionary<string, ILogger> loggers;
		public async Task Run(IServiceProvider provider)
		{
			services = provider;
			loggers = new ConcurrentDictionary<string, ILogger>();
			var factory = provider.GetRequiredService<ILoggerFactory>();

			generalLogger = factory.CreateLogger<ModatronBot>();
			commandLogger = factory.CreateLogger("Commands");

			client.Log += (m) => {
				var logger = loggers.GetOrAdd(m.Source, source => factory.CreateLogger(source));
				switch (m.Severity)
				{
					case LogSeverity.Critical:
						if (m.Exception != null)
							logger.LogCritical($"{m.Message}\n{m.Exception}");
						else
							logger.LogCritical(m.Message);
						break;
					case LogSeverity.Error:
						if (m.Exception != null)
							logger.LogError($"{m.Message}\n{m.Exception}");
						else
							logger.LogError(m.Message);
						break;
					case LogSeverity.Warning:
						if (m.Exception != null)
							logger.LogWarning($"{m.Message}\n{m.Exception}");
						else
							logger.LogWarning(m.Message);
						break;
					case LogSeverity.Info:
						if (m.Exception != null)
							logger.LogInformation($"{m.Message}\n{m.Exception}");
						else
							logger.LogInformation(m.Message);
						break;
					case LogSeverity.Verbose:
						if (m.Exception != null)
							logger.LogDebug($"{m.Message}\n{m.Exception}");
						else
							logger.LogDebug(m.Message);
						break;
					case LogSeverity.Debug:
						if (m.Exception != null)
							logger.LogTrace($"{m.Message}\n{m.Exception}");
						else
							logger.LogTrace(m.Message);
						break;
				}

				return Task.CompletedTask;
			};

			client.LatencyUpdated += HandleBanTicking;

			var messageLogger = factory.CreateLogger("Message");
			client.MessageReceived += (m) => {
				messageLogger.LogInformation(m.ToString());
				#pragma warning disable
				HandleMessage(m);
				#pragma warning restore
				return Task.CompletedTask;
			};

			HttpClient dapi = new HttpClient
			{
				BaseAddress = new Uri("https://bots.discord.pw/api/bots/")
			};

			dapi.DefaultRequestHeaders.Add("Authorization", Configuration.GetSection("tokens")["botlist"]);

			client.JoinedGuild += (m) => PostGuildStats(dapi);
			client.LeftGuild += (m) => PostGuildStats(dapi);
			client.Ready += () => PostGuildStats(dapi);

			await commands.LoadAssembly(Assembly.GetEntryAssembly(), new AspNetDependencyMap(provider));
			generalLogger.LogDebug($"Loaded {commands.Modules.Count()} modules containing {commands.Commands.Count()} commands"); 
			await client.LoginAsync(TokenType.Bot, Configuration.GetSection("tokens")["discord"]);
			await client.ConnectAsync();
			self = await client.GetCurrentUserAsync();
			startTime = DateTime.UtcNow;
			await Task.Delay(-1);
		}

		private async Task PostGuildStats(HttpClient postClient)
		{
			string json = "";
			if (TotalShards > 0)
				json = JsonConvert.SerializeObject(new {
					shard_id = ShardId,
					shard_count = TotalShards,
					server_count = client.GetGuilds().Count
				});
			else
				json = JsonConvert.SerializeObject(new {
					server_count = client.GetGuilds().Count
				});

			var response = await postClient.PostAsync("224227130538590208/stats", new StringContent(json, System.Text.Encoding.Unicode, "application/json"));
			response.EnsureSuccessStatusCode();
		}

		public void ConfigureServices(IServiceCollection services)
		{
			services.AddDbContext<PunishDbContext>();
			services.AddSingleton(this);
			services.AddSingleton(client);
			services.AddSingleton(commands);
			services.AddSingleton<MessageBuilder>();
			services.AddSingleton<MessageDeleter>();
			services.AddSingleton<PunishmentService>();
			services.AddSingleton<RoleService>();
		}
	}
}