using System;

namespace Modatron
{
	class CommandException : InvalidOperationException
	{
		public CommandException() : base() { }
		public CommandException(string message) : base(message) { }
		public CommandException(string message, Exception innerException) : base(message, innerException) { }
	}
}