using System;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;

using Modatron.Services;
using Modatron.Preconditions;

namespace Modatron.Commands
{
	[Module("reason"), Name("Reasons")]
	[RequireRole("Staff")]
	[Remarks("Manages per-guild punishment reasons")]
	public class ReasonCommands
	{
		private PunishmentService punishment;
		private MessageDeleter deleter;

		public ReasonCommands(
			PunishmentService _punish,
			MessageDeleter _deleter)
		{
			punishment = _punish;
			deleter = _deleter;
		}

		[Command, Name("Set reason")]
		[Remarks("Updates the reason for a punishment")]
		public async Task ReasonCommand(IUserMessage message, int caseNumber, [Remainder]string reason)
		{
			try
			{
				await punishment.UpdatePunishment(caseNumber, reason);
			}
			catch (InvalidOperationException e)
			{
				throw new CommandException(e.Message, e);
			}
			var successMessage = await message.Channel.SendMessageAsync("Updated reason.");
			await Task.Delay(5000);
			await deleter.DeleteMessages(successMessage, message);
		}

		[Command, Name("Set reason"), Alias("latest")]
		[Remarks("Updates the reason for a punishment")]
		public async Task ReasonCommand(IUserMessage message, [Remainder]string reason)
		{
			try
			{
				var caseNumber = punishment.GetLatestCase((message.Channel as ITextChannel).Guild);
				if (caseNumber > 0)
					await punishment.UpdatePunishment(caseNumber, reason);
			}
			catch (InvalidOperationException e)
			{
				throw new CommandException(e.Message, e);
			}
			var successMessage = await message.Channel.SendMessageAsync("Updated reason.");
			await Task.Delay(5000);
			await deleter.DeleteMessages(successMessage, message);
		}
	}
}