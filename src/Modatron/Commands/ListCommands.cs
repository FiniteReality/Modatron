using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

using Discord;
using Discord.Commands;

using Modatron.Models;
using Modatron.Services;
using Modatron.Preconditions;

namespace Modatron.Commands
{
	[Module("list"), Name("List punishments")]
	[Remarks("Lists per-guild punishments")]
	[RequireRole("Staff")]
	public class ListCommands
	{
		private PunishmentService punishment;
		private MessageBuilder builder;
		private MessageDeleter deleter;

		public ListCommands(
			PunishmentService _punish,
			MessageBuilder _builder,
			MessageDeleter _deleter)
		{
			punishment = _punish;
			builder = _builder;
			deleter = _deleter;
		}

		private async Task ListPunishments(IUserMessage message, Expression<Func<Punishment, bool>> filter)
		{
			var punishments = punishment.GetPunishments(filter);

			string builtMessage = "No punishments found matching the given filter.";

			if (punishments.FirstOrDefault() != null)
			{
				bool truncated = false;
				if (punishments.Count() > 20)
				{
					truncated = true;
					punishments = punishments.Take(20);
				}
				
				punishments = punishments.OrderBy(x => x.Id);

				StringBuilder messageBuilder = new StringBuilder("Found the following punishments:\n");
				foreach (var punish in punishments)
				{
					messageBuilder.AppendLine(builder.BuildPunishMessage(punish));
				}

				if (truncated)
					messageBuilder.AppendLine(
@"The list of punishments has been truncated to 20 items.
Please make your query more explicit to hide this message.");

				builtMessage = messageBuilder.ToString();
			}

			var channel = await (message.Author as IGuildUser).CreateDMChannelAsync();
			await channel.SendMessageAsync(builtMessage);

			var successMessage = await message.Channel.SendMessageAsync("A list of punishments has been DM'd to you.");
			await Task.Delay(5000);
			await deleter.DeleteMessages(successMessage, message);
		}

		[Command, Name("All punishments"), Alias("all")]
		[Remarks("Lists all punishments within the guild")]
		public async Task List(IUserMessage message)
		{
			var guild = (message.Author as IGuildUser).Guild;
			await ListPunishments(message, x => x.Guild.GuildId == guild.Id);
		}

		[Command("search"), Name("Punishments affecting many users")]
		[Remarks("Lists all punishments affecting the users within the guild")]
		public async Task ListSearch(IUserMessage message, string name)
		{
			var guild = (message.Author as IGuildUser).Guild;
			var users = (await guild.GetUsersAsync()).Where(x => (x.Nickname??x.Username).StartsWith(name)).Select(x => x.Id);
			await ListPunishments(message, x => users.Contains(x.TargetUser));
		}

		[Command("named"), Name("Punishments affecting a user"), Alias("user")]
		[Remarks("Lists all punishments affecting the user within the guild")]
		public async Task ListNamed(IUserMessage message, IGuildUser user)
		{
			await ListPunishments(message, x => x.TargetUser == user.Id);
		}
	}
}