using System;
using System.Linq;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;

using Modatron.Models;
using Modatron.Services;
using Modatron.Preconditions;

namespace Modatron.Commands
{
	[Module("configure"), Name("Configuration")]
	[RequireRole("Staff")]
	[Remarks("Manages per-guild Modatron configuration")]
	public class ConfigureCommands
	{
		private PunishDbContext database;
		private MessageDeleter deleter;

		public ConfigureCommands(
			PunishDbContext _database,
			MessageDeleter _deleter)
		{
			database = _database;
			deleter = _deleter;
		}

		[Command("log"), Name("Set log channel")]
		[Remarks("Sets the log channel for logging messages")]
		public async Task ConfigureLogChannel(IUserMessage message, ITextChannel logChannel)
		{
			try
			{
				var guild = (message.Channel as IGuildChannel).Guild;
				await UpdateConfiguration(guild, x => x.LogChannelId = logChannel.Id);
			}
			catch (Exception e)
			{
				throw new CommandException("Failed to update the log channel.", e);
			}

			var successMessage = await message.Channel.SendMessageAsync("Updated the log channel.");
			await Task.Delay(5000);
			await deleter.DeleteMessages(successMessage, message);
		}

		private async Task UpdateConfiguration(IGuild guild, Action<GuildConfiguration> update)
		{
			var config = database.Configurations.FirstOrDefault(x => x.GuildId == guild.Id);

			if (config == null)
			{
				config = new GuildConfiguration{
					GuildId = guild.Id
				};
				database.Configurations.Add(config);
			}

			update(config);

			await database.SaveChangesAsync();
		}
	}
}