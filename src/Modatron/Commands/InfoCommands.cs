using System.Threading.Tasks;

using Discord;
using Discord.Commands;
using Discord.WebSocket;

using Modatron.Attributes;
using Modatron.Extensions;

namespace Modatron.Commands
{
	[Module("info"), Name("Bot information")]
	[Remarks("Returns information about this bot")]
	public class InfoCommands
	{
		const string DBOTS_URL = "https://bots.discord.pw/bots/224227130538590208";

		const string SOURCE_URL = "https://gitlab.com/FiniteReality/Modatron";

		private DiscordSocketClient client;

		private ModatronBot bot;

		public InfoCommands(
			DiscordSocketClient _client,
			ModatronBot _bot)
		{
			client = _client;
			bot = _bot;
		}

		[Command, Name("General information")]
		[Remarks("Returns general information")]
		public async Task GeneralInfo(IUserMessage message, [HiddenParameter][Remainder]string asd = null)
		{
			var appInfo = await client.GetApplicationInfoAsync();

			await message.Channel.SendMessageAsync(
$@"**Hello!** I'm Modatron.
I was created by {appInfo.Owner} and I'm here to make moderation easy!

I have been running for {bot.Uptime().ToPrettyFormat()}
I am using Discord.Net {DiscordConfig.Version}
I am publicly available at <{SOURCE_URL}>

Need help? Any questions? Want me in your server?
Join the official server! {DBOTS_URL}");
		}
	}
}