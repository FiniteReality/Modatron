using System;
using System.Linq;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;

using Modatron.Models;
using Modatron.Services;
using Modatron.Preconditions;

namespace Modatron.Commands
{
	[Module("warn"), Name("Warning")]
	[RequireRole("Staff")]
	[Remarks("Manages per-guild warnings")]
	public class WarnCommands
	{
		private PunishmentService punishment;
		private MessageDeleter deleter;

		public WarnCommands(
			PunishmentService _punish,
			MessageDeleter _deleter)
		{
			punishment = _punish;
			deleter = _deleter;
		}

		[Command("remove"), Name("Remove warning"), Alias("delete", "del", "rem")]
		[Remarks("Removes the warning on the given user")]
		public async Task UnwarnUser(IUserMessage message, IGuildUser target, [Remainder]string reason = null)
		{
			try
			{
				await punishment.RemovePunishment(target, PunishmentType.Warning, message.Author as IGuildUser, reason);
			}
			catch (InvalidOperationException e)
			{
				throw new CommandException(e.Message, e);
			}
			catch (Exception e)
			{
				throw new CommandException("Unable to remove the warning.", e);
			}

			var successMessage = await message.Channel.SendMessageAsync("Unwarned user.");
			await Task.Delay(5000);
			await deleter.DeleteMessages(successMessage, message);
		}

		[Command, Name("Warning")]
		[Remarks("Warns a user for 5 minutes with the given reason")]
		public async Task WarnUser(IUserMessage message, IGuildUser target, [Remainder]string reason)
		{
			await WarnUserTimedDescription(message, target, TimeSpan.FromMinutes(5), reason);
		}

		[Command, Name("Timed warning")]
		[Remarks("Warns a user with the given time and reason")]
		public async Task WarnUserTimedDescription(IUserMessage message, IGuildUser target, TimeSpan time, [Remainder]string reason)
		{
			await WarnUserInternal(message.Channel, message.Author as IGuildUser, target, time, reason);
			var successMessage = await message.Channel.SendMessageAsync("Warned user.");
			await Task.Delay(5000);
			await deleter.DeleteMessages(successMessage, message);
		}

		private async Task WarnUserInternal(IMessageChannel channel, IGuildUser invoker, IGuildUser target, TimeSpan time, string reason)
		{
			if (time < TimeSpan.FromMinutes(1))
				throw new CommandException("Warnings must be at least 1 minute!");

			if (time > TimeSpan.FromDays(3)) //TODO: make this a configurable thing
				throw new CommandException("Warnings must be less than 3 days!");

			if (invoker.Guild.OwnerId != invoker.Id)
				if (invoker.Roles.Max(x => x.Position) <= target.Roles.Max(x => x.Position))
					throw new CommandException("You cannot warn somebody with the same permissions as you!");
			
			try
			{
				await punishment.AddWarning(channel, target, invoker, time, reason);
			}
			catch (InvalidOperationException e)
			{
				throw new CommandException(e.Message, e);
			}
			catch (Exception e)
			{
				throw new CommandException("Unable to warn the user.", e);
			}
		}
	}
}