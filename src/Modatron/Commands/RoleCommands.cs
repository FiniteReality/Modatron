using System;
using System.Linq;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;

using Modatron.Models;
using Modatron.Services;
using Modatron.Preconditions;

namespace Modatron.Commands
{
	[Module("role"), Name("Autorole")]
	[Remarks("Manages per-guild autoroles")]
	public class RoleCommands
	{		
		private RoleService service;
		private MessageDeleter deleter;

		public RoleCommands(
			RoleService _service,
			MessageDeleter _deleter)
		{
			service = _service;
			deleter = _deleter;
		}

		[Command("deconfig"), Name("Delete autorole"), Alias("deconfigure")]
		[Remarks("Deletes an autorole")]
		[RequireRole("Staff")]
		public async Task DeconfigRole(IUserMessage message, string roleName)
		{
			try
			{
				await service.DeleteRole(message.Author as IGuildUser, roleName);
			}
			catch (InvalidOperationException e)
			{
				throw new CommandException(e.Message, e);
			}

			var successMessage = await message.Channel.SendMessageAsync($"Successfully deleted `{roleName}`");
			await Task.Delay(5000);
			await deleter.DeleteMessages(successMessage, message);
		}

		[Group("config"), Name("Configure autoroles")]
		[Remarks("Configures an autorole")]
		public class ConfigureGroup
		{
			private RoleService service;
			private MessageDeleter deleter;

			public ConfigureGroup(
				RoleService _service,
				MessageDeleter _deleter)
			{
				service = _service;
				deleter = _deleter;
			}

			[Command, Name("Configure role"), Alias("role")]
			[Remarks("Configures the role an autorole is attached to")]
			[RequireRole("Staff")]
			public async Task ConfigureRole(IUserMessage message, string roleName, IRole role)
			{
				var user = message.Author as IGuildUser;
				if (user.Roles.Max(x => x.Position) <= role.Position)
					throw new CommandException($"Autoroles must be more restricted than yours");

				try
				{
					await service.CreateOrModifyRole(message.Author as IGuildUser, roleName, x=> x.RoleId = role.Id);
				}
				catch (InvalidOperationException e)
				{
					throw new CommandException(e.Message, e);
				}

				var successMessage = await message.Channel.SendMessageAsync($"Successfully configured `{roleName}`");
				await Task.Delay(5000);
				await deleter.DeleteMessages(successMessage, message);
			}

			[Command, Name("Configure public"), Alias("public")]
			[Remarks("Configures whether an autorole is public or not")]
			[RequireRole("Staff")]
			public async Task ConfigurePublic(IUserMessage message, string roleName, AutorolePublicity publicity)
			{
				try
				{
					await service.CreateOrModifyRole(message.Author as IGuildUser, roleName, x => x.Publicity = publicity);
				}
				catch (InvalidOperationException e)
				{
					throw new CommandException(e.Message, e);
				}

				var successMessage = await message.Channel.SendMessageAsync($"Successfully configured `{roleName}`");
				await Task.Delay(5000);
				await deleter.DeleteMessages(successMessage, message);
			}
		}

		[Command("add"), Name("Add autorole"), Alias("assign")]
		[Remarks("Adds an autorole to the given users")]
		[RequireRole("Staff")]
		public async Task AddRole(IUserMessage message, string roleName, params IGuildUser[] users)
		{
			try
			{
				foreach (var user in users)
					await service.AddRole(user, roleName, message.Author as IGuildUser);
			}
			catch (InvalidOperationException e)
			{
				throw new CommandException(e.Message, e);
			}

			var successMessage = await message.Channel.SendMessageAsync($"Successfully added `{roleName}` to {users.Length} users");
			await Task.Delay(5000);
			await deleter.DeleteMessages(successMessage, message);
		}

		[Command("get"), Name("Get an autorole")]
		[Remarks("Toggles the autorole on yourself")]
		public async Task ToggleRole(IUserMessage message, [Remainder]string roleName)
		{
			try
			{
				await service.ToggleRole(message.Author as IGuildUser, roleName, message.Author as IGuildUser);
			}
			catch (InvalidOperationException e)
			{
				throw new CommandException(e.Message, e);
			}

			var successMessage = await message.Channel.SendMessageAsync($"Successfully toggled the `{roleName}` autorole");
			await Task.Delay(5000);
			await deleter.DeleteMessages(successMessage, message);
		}

		[Command("delete"), Name("Remove autorole"), Alias("del", "remove")]
		[Remarks("Removes an autorole from the given users")]
		[RequireRole("Staff")]
		public async Task DeleteAutoRole(IUserMessage message, string roleName, params IGuildUser[] users)
		{
			try
			{
				foreach (var user in users)
					await service.RemoveRole(user, roleName, message.Author as IGuildUser);
			}
			catch (InvalidOperationException e)
			{
				throw new CommandException(e.Message, e);
			}

			var successMessage = await message.Channel.SendMessageAsync($"Successfully removed `{roleName}` from {users.Length} users");
			await Task.Delay(5000);
			await deleter.DeleteMessages(successMessage, message);
		}
	}
}