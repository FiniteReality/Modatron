using System;
using System.Linq;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;

using Modatron.Models;
using Modatron.Services;
using Modatron.Preconditions;

namespace  Modatron.Commands
{
	[Module("ban"), Name("Banning")]
	[RequireRole("Staff")]
	[Remarks("Manages per-guild bans")]
	public class BanCommands
	{
		private PunishmentService punishment;
		private MessageDeleter deleter;

		public BanCommands(
			PunishmentService _punish,
			MessageDeleter _deleter)
		{
			punishment = _punish;
			deleter = _deleter;
		}

		public async Task UnbanUser(IUserMessage message, IGuildUser target, [Remainder]string reason = null)
		{
			try
			{
				await punishment.RemovePunishment(target, PunishmentType.Ban, message.Author as IGuildUser, reason);
			}
			catch (InvalidOperationException e)
			{
				throw new CommandException(e.Message, e);
			}
			catch (Exception e)
			{
				throw new CommandException("Unable to remove the ban.", e);
			}

			var successMessage = await message.Channel.SendMessageAsync("Unbanned user.");
			await Task.Delay(5000);
			await deleter.DeleteMessages(successMessage, message);
		}

		[Command, Name("Ban user")]
		[Remarks("Bans a user for half an hour with the specified reason.")]
		public async Task BanUser(IUserMessage message, IGuildUser target, [Remainder]string reason)
		{
			await BanUserTemp(message, target, TimeSpan.FromMinutes(30), reason);
		}

		[Command, Name("Ban user with time"), Alias("timed")]
		[Remarks("Bans a user with the specified time and reason.")]
		public async Task BanUserTemp(IUserMessage message, IGuildUser target, TimeSpan time, [Remainder]string reason)
		{
			await BanUserInternal(message, message.Author as IGuildUser, target, time, reason);

			var successMessage = await message.Channel.SendMessageAsync("Banned user.");
			await Task.Delay(5000);
			await message.Channel.DeleteMessagesAsync(new IMessage[]{successMessage, message});
		}

		[Command("perm"), Name("Permanently ban user")]
		[Remarks("Permanently bans a user with the given reason.")]
		public async Task PermBanUser(IUserMessage message, IGuildUser target, [Remainder]string reason)
		{
			var invoker = message.Author as IGuildUser;

			if (invoker.Guild.OwnerId != invoker.Id)
				if (invoker.GuildPermissions.RawValue <= target.GuildPermissions.RawValue)
					throw new CommandException("You cannot ban somebody with the same permissions as you!");

			try
			{
				await punishment.AddPermBan(target, invoker, reason);
			}
			catch (InvalidOperationException e)
			{
				throw new CommandException(e.Message, e);
			}
			catch (Exception e)
			{
				throw new CommandException("Unable to ban user", e);
			}

			var successMessage = await message.Channel.SendMessageAsync("Banned user.");
			await Task.Delay(5000);
			await deleter.DeleteMessages(successMessage, message);
		}

		private async Task BanUserInternal(IUserMessage message, IGuildUser invoker, IGuildUser target, TimeSpan time, string reason)
		{
			if (invoker.Guild.OwnerId != invoker.Id)
				if (invoker.Roles.Max(x => x.Position) <= target.Roles.Max(x => x.Position))
					throw new CommandException("You cannot ban somebody with the same permissions as you!");

			try
			{
				await punishment.AddTempBan(target, invoker, time, reason);
			}
			catch (InvalidOperationException e)
			{
				throw new CommandException(e.Message, e);
			}
			catch (Exception e)
			{
				throw new CommandException("Unable to ban user", e);
			}
		}
	}
}