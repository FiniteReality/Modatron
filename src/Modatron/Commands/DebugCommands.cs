using System;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

using Discord;
using Discord.Commands;
using Discord.WebSocket;

using Modatron.Models;
using Modatron.Preconditions;

namespace Modatron.Commands
{
	[Module("debug"), Name("Debugging")]
	[Remarks("Debugging commands for developers")]
	[RequireOwner]
	public class DebugCommands
	{
		private CommandService commands;
		private DiscordSocketClient client;
		private ModatronBot bot;
		private PunishDbContext context;

		public DebugCommands(
			CommandService _commands,
			DiscordSocketClient _client,
			ModatronBot _bot,
			PunishDbContext _context)
		{
			context = _context;
			bot = _bot;
			commands = _commands;
			client = _client;
		}

		[Command("stats"), Alias("status"), Name("Bot status")]
		[Remarks("Returns the current status of the bot")]
		public async Task Status(IUserMessage message)
		{
			var commit = "";
			using (var proc = System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo("git", "log --format=\"%h - %s\" HEAD~1..HEAD") {
				RedirectStandardOutput = true
			}))
			{
				commit = await proc.StandardOutput.ReadLineAsync();
				await Task.Run(() => proc.WaitForExit());
			}

			if (commit.Length < 0)
				commit = "unknown";

			var guilds = client.GetGuilds();
			var channels = guilds.Sum(x => x.GetChannels().Count);
			var usersReal = guilds.Sum(x => x.GetUserCount());
			var usersCached = guilds.Sum(x => x.GetCachedUserCount());

			var heapSize = (GC.GetTotalMemory(true) / 1024.0 / 1024.0);
			var activePunishments = context.Punishments.Count();
			var backend = context.Database.GetDbConnection().GetType();
			await message.Channel.SendMessageAsync(
$@"**Bot status**
**Commit**: {commit}
**Uptime**: {bot.Uptime()}
**Heap size**: {heapSize:0.00} MB
**Active Punishments**: {activePunishments}
**Estimated RTT**: {client.Latency} ms
**Library**: Discord.Net {DiscordSocketConfig.Version}
**OS**: {System.Runtime.InteropServices.RuntimeInformation.OSDescription}
**DB Connection**: {backend}
**Cached counts**: {guilds.Count} guilds, {channels} channels, {usersReal} users, {usersCached} cached users");
		}

		[Command("gc"), Alias("collect", "collectgarbage"), Name("Garbage collect")]
		[Remarks("Forces a garbage collection on the bot and returns the current status")]
		public async Task CollectGarbage(IUserMessage message)
		{
			GC.Collect();
			await Status(message);
		}

		[Command("punishments"), Name("Get punishments")]
		[Remarks("Returns the currently active punishments")]
		public Task GetAllPunishments(IUserMessage message)
		{
			return PunishmentsMessage(message, x => true);
		}

		[Command("punishments"), Name("Get typed punishments")]
		[Remarks("Returns the currently active punishments with the given type")]
		public Task GetTypedPunishments(IUserMessage message, PunishmentType type)
		{
			return PunishmentsMessage(message, x => x.Type == type);
		}

		[Command("punishments"), Name("Get user punishments")]
		[Remarks("Returns the currently active punishments for a given user")]
		public Task GetPunishments(IUserMessage message, IGuildUser user)
		{
			return PunishmentsMessage(message, x => x.TargetUser == user.Id);
		}

		[Command("userinfo"), Name("Get User Info"), Alias("uinfo")]
		[Remarks("Returns the information for the gievn user")]
		public async Task GetUserInfo(IUserMessage message, IGuildUser user)
		{
			await message.Channel.SendMessageAsync(
$@"User information:
**Account**: {Format.Sanitize(user.Username)}#{user.Discriminator}{(user.IsBot ? " BOT" : "")} ({user.Id})
**Nickname**: {Format.Sanitize(user.Nickname ?? "No Nickname")}
**Avatar**: <{user.AvatarUrl}>
**Creation Date**: {user.CreatedAt}
**Join Date**: {user.JoinedAt}
**Roles**: {string.Join(", ", user.Roles.Select(x => Format.Sanitize(x.Name.Replace("@", "@\x200b"))))}
**Permissions**: {user.GuildPermissions.RawValue}
**Voice Status**: {((user.IsDeafened || user.IsSelfDeafened) ? "Deafened" : "Not Deafened")} - {((user.IsMuted || user.IsSelfMuted) ? "Muted" : "Not Muted")} - {(user.IsSuppressed ? "Suppressed" : "Not Suppressed")}
**Status**: {user.Status}
**Game**: {Format.Sanitize(user.Game?.Name ?? "Not Playing")} - {user.Game?.StreamType.ToString() ?? "Not Streaming"}");
		}

		[Command("drop punishment"), Name("Drop punishment")]
		[Remarks("Drops a punishment. Use only in emergencies!")]
		public async Task DropPunishment(IUserMessage message, int punishmentId, [Remainder]string reason)
		{
			var punishment = context.Punishments.FirstOrDefault(x => x.Id == punishmentId);
			if (punishment == null)
				throw new CommandException($"Could not find punishment with id {punishmentId}");

			try
			{
				if (string.IsNullOrWhiteSpace(reason))
					throw new CommandException($"A reason is required to use this command.");

				var guildConfig = punishment.Guild;
				var logChannel = await client.GetChannelAsync(guildConfig.LogChannelId) as ITextChannel;

				var user = punishment.TargetUser == 0 ? null : await client.GetUserAsync(punishment.TargetUser);
				var channel = punishment.ChannelId == 0 ? null : await client.GetChannelAsync(punishment.ChannelId);

				await logChannel.SendMessageAsync(
$@"**Punishment Dropped** | Case {punishmentId}
**User**: {(user?.ToString() ?? "Unknown User")} ({punishment.ResponsibleUser})
**Channel**: {(channel?.ToString() ?? "Unknown Channel")} ({punishment.ChannelId})
**Reason**: {reason}
This action may have prevented a punishment from being removed; please check manually to ensure that the punishment has been removed.

Not sure why you're seeing this message?
This message means that FiniteReality has manually deleted a punishment in your guild - probably due to a bug.
(Personally, as the owner, I apologise for this - please remember to report bugs as soon as you find them to ensure they are fixed!)");
			}
			finally
			{
				context.Punishments.Remove(punishment);
				await context.SaveChangesAsync();
			}

			await message.Channel.SendMessageAsync("Successfully dropped punishment.");
		}

		private async Task PunishmentsMessage(IUserMessage message, Expression<Func<Punishment, bool>> query)
		{
			StringBuilder messageBuilder = new StringBuilder("Found the following punishments:\n");
			var punishments = context.Punishments.Where(query).OrderByDescending(x => x.Time);

			foreach (Punishment punishment in punishments)
			{
				messageBuilder.AppendLine($"**Case {punishment.Id} | {punishment.Type}**: {punishment.TargetUser} by {punishment.ResponsibleUser} at {punishment.Time}");
			}

			await message.Channel.SendMessageAsync(messageBuilder.ToString());
		}
	}
}