using System.Linq;
using System.Text;
using System.Reflection;
using System.Threading.Tasks;

using Discord;
using Discord.WebSocket;
using Discord.Commands;

using Modatron.Services;
using Modatron.Attributes;

namespace Modatron.Commands
{
	[Module("help"), Name("Help")]
	[Remarks("Usage and command syntax")]
	public class HelpCommands
	{
		private DiscordSocketClient client;
		private CommandService cmdService;
		private MessageDeleter deleter;

		public HelpCommands(
			DiscordSocketClient _client,
			CommandService _commands,
			MessageDeleter _deleter)
		{
			client = _client;
			cmdService = _commands;
			deleter = _deleter;
		}

		[Command, Name("All commands")]
		[Remarks("Returns a list of commands, similar to this message you're seeing right now.")]
		public async Task Help(IUserMessage message)
		{
			var helpMessage = await BuildHelpMessage(message, null, true);
			helpMessage = $"{helpMessage}\nCouldn't find what you were looking for? Join the help server! https://bots.discord.pw/bots/224227130538590208";

			if (message.Author is IGuildUser)
			{
				var channel = await (message.Author as IGuildUser).CreateDMChannelAsync();
				await channel.SendMessageAsync(helpMessage);

				var successMessage = await message.Channel.SendMessageAsync("Help commands have been DM'd to you!");
				await Task.Delay(5000);
				await deleter.DeleteMessages(successMessage, message);
			}
			else
			{
				await message.Channel.SendMessageAsync(helpMessage);
			}
		}

		[Command, Name("Specific commands")]
		[Remarks("Returns a list of commands matching the query requested")]
		public async Task HelpSpecific(IUserMessage message, [Remainder]string query)
		{
			var helpMessage = await BuildHelpMessage(message, query);
			helpMessage = $"{helpMessage}\nCouldn't find what you were looking for? Join the help server! https://bots.discord.pw/bots/224227130538590208";

			if (message.Author is IGuildUser)
			{
				var channel = await (message.Author as IGuildUser).CreateDMChannelAsync();
				await channel.SendMessageAsync(helpMessage);

				var successMessage = await message.Channel.SendMessageAsync("Help commands have been DM'd to you!");
				await Task.Delay(5000);
				await deleter.DeleteMessages(successMessage, message);
			}
			else
			{
				await message.Channel.SendMessageAsync(helpMessage);
			}
		}

		private async Task<string> BuildHelpMessage(IUserMessage context, string query, bool consise = false)
		{
			StringBuilder response = new StringBuilder();
			ILookup<Discord.Commands.Module, Command> matchingCommands = null;

			string prefix = "All commands:";

			if (string.IsNullOrWhiteSpace(query))
			{
				matchingCommands = cmdService.Commands.ToLookup(x => x.Module);
			}
			else
			{
				prefix = $"Commands matching the query `{Format.Sanitize(query)}`:";
				matchingCommands = cmdService.Commands.Where(x=>x.Aliases.Any(y=>y.StartsWith(query))).ToLookup(x => x.Module);
			}

			
			foreach (var grouping in matchingCommands)
			{
				StringBuilder moduleResponse = new StringBuilder();

				if (consise)
				{
					bool canUse = true;
					foreach (var command in grouping)
					{
						var status = await command.CheckPreconditions(context);
						if (!status.IsSuccess)
						{
							canUse = false;
							break;
						}
					}

					if (canUse)
						moduleResponse.AppendLine($"See `.help {grouping.Key.Prefix}`");
				}
				else
				{
					foreach (var command in grouping)
					{
						var canUse = await command.CheckPreconditions(context);
						if (canUse.IsSuccess)
						{
							moduleResponse.AppendLine($"\t{command.Name}: {command.Remarks}");
							moduleResponse.Append("\t`");
							moduleResponse.Append(command.Aliases.First());
							moduleResponse.Append(" ");

							foreach (var parameter in command.Parameters)
							{
								if (parameter.Source.GetCustomAttribute<HiddenParameterAttribute>() != null)
									continue;
								
								if (parameter.IsOptional || parameter.DefaultValue != null)
									moduleResponse.Append("[");

								moduleResponse.Append(parameter.Name);

								if (parameter.DefaultValue != null)
									moduleResponse.Append($" = {Format.Sanitize(parameter.DefaultValue.ToString())}");

								if (parameter.IsRemainder)
									moduleResponse.Append("...");

								if (parameter.IsOptional || parameter.DefaultValue != null)
									moduleResponse.Append("]");

								if (parameter.IsMultiple)
									moduleResponse.Append("...");

								moduleResponse.Append(" ");
							}

							moduleResponse.AppendLine("`");
						}
					}
				}

				if (moduleResponse.Length > 0)
				{
					response.AppendLine($"**{grouping.Key.Name}**: {grouping.Key.Remarks}");
					response.AppendLine(moduleResponse.ToString());
				}
			}

			if (response.Length > 0)
				return $"{prefix}\n{response.ToString()}";
			else
				return $"Could not find any usable commands matching the query `{Format.Sanitize(query)}`";
		}
	}
}