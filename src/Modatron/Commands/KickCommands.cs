using System;
using System.Linq;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;

using Modatron.Services;
using Modatron.Preconditions;

namespace Modatron.Commands
{
	[Module("kick"), Name("Kicking")]
	[RequireRole("Staff")]
	[Remarks("Manages per-guild kicking")]
	public class KickCommands
	{
		private PunishmentService punishment;
		private MessageDeleter deleter;

		public KickCommands(
			PunishmentService _punish,
			MessageDeleter _deleter)
		{
			punishment = _punish;
			deleter = _deleter;
		}

		[Command, Name("Kick user")]
		[Remarks("Kicks the user from the guild with the given reason")]
		public async Task KickUser(IUserMessage message, IGuildUser target, [Remainder]string reason)
		{
			var invoker = message.Author as IGuildUser;

			if (invoker.Guild.OwnerId != invoker.Id)
				if (invoker.Roles.Max(x => x.Position) <= target.Roles.Max(x => x.Position))
					throw new CommandException("You cannot kick somebody with the same permissions as you!");

			try
			{
				await punishment.AddSoftban(target, invoker, reason);
			}
			catch (InvalidOperationException e)
			{
				throw new CommandException(e.Message, e);
			}
			catch (Exception e)
			{
				throw new CommandException("Unable to kick the user.", e);
			}

			var successMessage = await message.Channel.SendMessageAsync("Kicked user.");
			await Task.Delay(5000);
			await deleter.DeleteMessages(successMessage, message);
		}
	}
}