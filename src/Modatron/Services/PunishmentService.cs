using System;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;

using Microsoft.Extensions.Logging;

using Discord;
using Discord.WebSocket;

using Modatron.Models;

namespace Modatron.Services
{
	public class PunishmentService
	{
		private PunishDbContext database;
		private DiscordSocketClient client;
		private ILogger logger;
		private MessageBuilder builder;

		public PunishmentService(
			PunishDbContext _database,
			DiscordSocketClient _client,
			ILogger<PunishmentService> _logger,
			MessageBuilder _builder)
		{
			database = _database;
			client = _client;
			logger = _logger;
			builder = _builder;
		}

		public async Task Tick()
		{
			bool anyRemoved = false;
			foreach (var punishment in database.TimedPunishments)
			{
				DateTime expiry = (punishment.Time + (punishment as TimedPunishment).Duration).ToUniversalTime();
				logger.LogTrace($"Punishment: {punishment.Id} Expires: {expiry} Expired: {DateTime.UtcNow > expiry}");

				if (DateTime.UtcNow > expiry)
				{
					var config = punishment.Guild;
					if (config != null)
					{
						var channel = await client.GetChannelAsync(config.LogChannelId) as ITextChannel;

						await RemovePunishmentEffects(punishment);

						await channel.SendMessageAsync(builder.BuildExpireMessage(punishment));
					}

					database.TimedPunishments.Remove(punishment);
					anyRemoved = true;

					logger.LogDebug($"Punishment removed: {punishment.Id}");
				}
			}

			if (anyRemoved)
				await database.SaveChangesAsync();

			logger.LogDebug("Punishments ticked.");
		}

		public IQueryable<Punishment> GetPunishments(Expression<Func<Punishment, bool>> filter)
		{
			return database.Punishments.Where(filter);
		}

		public int GetLatestCase(IGuild guild)
		{
			var punishment = database.Punishments
				.Where(x => x.Guild.GuildId == guild.Id)
				.OrderByDescending(x => x.Id)
				.FirstOrDefault();

			return punishment?.Id ?? -1;
		}

		public Task RemovePunishment(IGuildUser target, PunishmentType type, IGuildUser responsible, string reason)
		{
			if (type == PunishmentType.Softban)
				throw new InvalidOperationException("Cannot remove softbans");

			var guild = database.Configurations.FirstOrDefault(x => x.GuildId == target.Guild.Id);
			if (guild == null)
				throw new InvalidOperationException("The guild must first be configured before adding a warning");

			var punishment = database.Punishments.FirstOrDefault(x => x.Guild.GuildId == guild.GuildId && x.TargetUser == target.Id && x.Type == type);

			if (punishment == null)
				throw new InvalidOperationException("That user does not have any punishments");

			if (punishment.ResponsibleUser == target.Guild.OwnerId && responsible.Id != target.Guild.OwnerId)
				throw new InvalidOperationException("You cannot remove punishments the guild owner has set!");

			return RemovePunishment(punishment, responsible, reason);
		}

		public async Task UpdatePunishment(int punishmentId, string newReason)
		{
			var punishment = database.Punishments.FirstOrDefault(x => x.Id == punishmentId);

			if (punishment == null)
				throw new InvalidOperationException($"Could not find a punishment with id {punishmentId}");

			var guildConfig = database.Configurations.FirstOrDefault(x => x.GuildId == punishment.Guild.GuildId);

			if (guildConfig == null)
				throw new InvalidOperationException("The guild must first be configured before updating a warning");

			if (!string.IsNullOrWhiteSpace(newReason))
			{
				punishment.Reason = newReason;

				var guild = await client.GetGuildAsync(punishment.Guild.GuildId);
				var channel = await guild.GetChannelAsync(guildConfig.LogChannelId) as ITextChannel;

				var message = await channel.GetMessageAsync(punishment.MessageId) as IUserMessage;

				await message.ModifyAsync(x=> x.Content = builder.BuildPunishMessage(punishment));
			}

			await database.SaveChangesAsync();
		}
		
		public Task AddWarning(IMessageChannel channel, IGuildUser target, IGuildUser responsible, TimeSpan time, string reason)
		{
			var guild = database.Configurations.FirstOrDefault(x => x.GuildId == target.Guild.Id);
			if (guild == null)
				throw new InvalidOperationException("The guild must first be configured before adding a warning");
			
			return AddPunishment(guild, new TimedPunishment{
				Type = PunishmentType.Warning,
				TargetUser = target.Id,
				ResponsibleUser = responsible.Id,
				Guild = guild,
				ChannelId = channel.Id,
				Duration = time,
				Reason = reason
			});
		}

		public Task AddSoftban(IGuildUser target, IGuildUser responsible, string reason)
		{
			var guild = database.Configurations.FirstOrDefault(x => x.GuildId == target.Guild.Id);
			if (guild == null)
				throw new InvalidOperationException("The guild must first be configured before adding a kick");

			return AddPunishment(guild, new Punishment{
				Type = PunishmentType.Softban,
				TargetUser = target.Id,
				ResponsibleUser = responsible.Id,
				Guild = guild,
				Reason = reason
			});
		}

		public Task AddTempBan(IGuildUser target, IGuildUser responsible, TimeSpan time, string reason)
		{
			var guild = database.Configurations.FirstOrDefault(x => x.GuildId == target.Guild.Id);
			if (guild == null)
				throw new InvalidOperationException("The guild must first be configured before adding a temporary ban");

			return AddPunishment(guild, new TimedPunishment{
				Type = PunishmentType.Ban,
				TargetUser = target.Id,
				ResponsibleUser = responsible.Id,
				Guild = guild,
				Duration = time,
				Reason = reason
			});
		}

		public Task AddPermBan(IGuildUser target, IGuildUser responsible, string reason)
		{
			var guild = database.Configurations.FirstOrDefault(x => x.GuildId == target.Guild.Id);
			if (guild == null)
				throw new InvalidOperationException("The guild must first be configured before adding a permanent ban");

			return AddPunishment(guild, new Punishment{
				Type = PunishmentType.Ban,
				TargetUser = target.Id,
				ResponsibleUser = responsible.Id,
				Guild = guild,
				Reason = reason
			});
		}

		private async Task RemovePunishment(Punishment punishment, IUser responsible, string reason)
		{
			var guildConfig = punishment.Guild;
			database.Punishments.Remove(punishment);
			await database.SaveChangesAsync();

			var logChannel = await client.GetChannelAsync(guildConfig.LogChannelId) as ITextChannel;

			await RemovePunishmentEffects(punishment);

			await logChannel.SendMessageAsync(builder.BuildRemoveMessage(responsible, punishment, reason));
		}

		private async Task RemovePunishmentEffects(Punishment punishment)
		{
			var guildConfig = punishment.Guild;
			var guild = await client.GetGuildAsync(guildConfig.GuildId);

			if (punishment.Type == PunishmentType.Warning)
			{
				var channel = await guild.GetChannelAsync(punishment.ChannelId);
				if (channel != null)
				{
					var user = await client.GetUserAsync(punishment.TargetUser);
					var perms = channel.GetPermissionOverwrite(user)?.Modify(sendMessages: PermValue.Inherit);
					
					if (perms.HasValue)
						if (perms?.AllowValue == perms?.DenyValue)
							await channel.RemovePermissionOverwriteAsync(user);
						else
							await channel.AddPermissionOverwriteAsync(user, perms.Value);
				}
			}
			else if (punishment.Type == PunishmentType.Ban)
			{
				var user = await client.GetUserAsync(punishment.TargetUser);
				await guild.RemoveBanAsync(user);
			}
		}

		private async Task AddPunishment(GuildConfiguration guildConfig, Punishment punishment)
		{
			logger.LogInformation($"Adding punishment: {punishment.TargetUser}");

			if (punishment.TargetUser == client.GetCurrentUser().Id)
				throw new InvalidOperationException("You cannot punish Modatron - it will result in undefined behaviour.");

			if (punishment.Type != PunishmentType.Softban)
			{
				var existingPunishment = database.Punishments.FirstOrDefault(x => x.Guild.GuildId == guildConfig.GuildId && x.TargetUser == punishment.TargetUser && x.Type == punishment.Type);
				if (existingPunishment != null)
					throw new InvalidOperationException($"This user has already received a {punishment.Type.ToString().ToLower()}!");
			}

			database.Punishments.Add(punishment);
			await database.SaveChangesAsync();

			var guild = await client.GetGuildAsync(guildConfig.GuildId);
			var logChannel = await guild.GetChannelAsync(guildConfig.LogChannelId) as ITextChannel;

			var messageContents = builder.BuildPunishMessage(punishment);

			if (punishment.Type == PunishmentType.Warning)
			{
				var channel = guild.GetChannel(punishment.ChannelId);
				var user = guild.GetUser(punishment.TargetUser);
				var perms = (channel.GetPermissionOverwrite(user) ?? OverwritePermissions.InheritAll).Modify(sendMessages: PermValue.Deny);
				await channel.AddPermissionOverwriteAsync(user, perms);
			}
			else if (punishment.Type == PunishmentType.Softban)
			{
				var user = await guild.GetUserAsync(punishment.TargetUser);
				await user.KickAsync();
			}
			else if (punishment.Type == PunishmentType.Ban)
			{
				await guild.AddBanAsync(punishment.TargetUser, 5);
			}

			var message = await logChannel.SendMessageAsync(messageContents);
			punishment.MessageId = message.Id;
			await database.SaveChangesAsync();
		}
	}
}