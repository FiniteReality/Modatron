using System;
using System.Text;

using Discord;
using Discord.WebSocket;

using Modatron.Models;

namespace Modatron.Services
{
	public class MessageBuilder
	{
		private DiscordSocketClient client;

		public MessageBuilder(DiscordSocketClient _client)
		{
			client = _client;
		}

		public string BuildRoleModifyMessage(bool addOrRemove, IUser target, IUser responsible, IRole role)
		{

			if (responsible.Id == target.Id)
				return
$@"**Autorole changed**
**User**: {target} ({target.Id})
**Role {(addOrRemove ? "added" : "removed" )}**: {role}";
			else
				return
$@"**Role Changed**
**User**: {target} ({target.Id})
**Role {(addOrRemove ? "added" : "removed" )}**: {role}
**Staff**: {responsible}";
		}

		public string BuildRoleDeleteMessage(IUser responsible, RoleConfiguration roleConfig)
		{
			return
$@"**Role Deleted** | Role {roleConfig.Id}
**User**: {responsible}
**Configured name**: {roleConfig.Name}";
		}

		public string BuildRoleConfigureMessage(IUser responsible, RoleConfiguration roleConfig, IRole role)
		{
			return
$@"**Role Configured** | Role {roleConfig.Id}
**User**: {responsible}
**Role**: {role}
**Configured name**: {roleConfig.Name}
**Publicity**: {roleConfig.Publicity}";
		}

		public string BuildPunishMessage(Punishment punishment)
		{
			var target = client.GetUser(punishment.TargetUser);
			var channel = client.GetChannel(punishment.ChannelId);
			var responsible = client.GetUser(punishment.ResponsibleUser);

			StringBuilder result = new StringBuilder();
			result.AppendLine($"**{punishment.Type}** | Case {punishment.Id}");
			result.AppendLine($"**User**: {target} ({target?.Id ?? punishment.TargetUser})");

			if (punishment.Type == PunishmentType.Warning)
			{
				result.AppendLine($"**Channel**: {channel} ({channel?.Id ?? punishment.ChannelId})");
			}

			result.AppendLine($"**Staff**: {responsible}");

			if (punishment is TimedPunishment)
			{
				DateTime expiry = (punishment.Time + (punishment as TimedPunishment).Duration).ToUniversalTime();
				string expiredMessage = DateTime.UtcNow > expiry ? "Expired" : "Valid";
				result.AppendLine($"**Expires**: {expiry} UTC ({expiredMessage})");
			}
			else if (punishment.Type == PunishmentType.Ban)
			{
				result.AppendLine($"**Expires**: Never");
			}

			result.AppendLine($"**Reason**: {punishment.Reason}");

			return result.ToString();
		}

		public string BuildExpireMessage(Punishment punishment)
		{
			var target = client.GetUser(punishment.TargetUser);
			var responsible = client.GetUser(punishment.ResponsibleUser);

			StringBuilder result = new StringBuilder();
			result.AppendLine($"**{punishment.Type} Expired** | Case {punishment.Id}");
			result.AppendLine($"**User**: {target} ({target?.Id ?? punishment.TargetUser})");
			result.AppendLine($"**Responsible user**: {responsible}");

			
			return result.ToString();
		}

		public string BuildRemoveMessage(IUser responsible, Punishment punishment, string reason)
		{
			var target = client.GetUser(punishment.TargetUser);

			StringBuilder result = new StringBuilder();
			result.AppendLine($"**{punishment.Type} Removed** | Case {punishment.Id}");
			result.AppendLine($"**User**: {target} ({target?.Id ?? punishment.TargetUser})");
			result.AppendLine($"**Staff**: {responsible}");
			if (!string.IsNullOrWhiteSpace(reason))
				result.AppendLine($"**Reason**: {reason}");

			return result.ToString();
		}
	}
}