using System;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Discord;
using Discord.WebSocket;

using Modatron.Models;

namespace Modatron.Services
{
	public class RoleService
	{
		private DiscordSocketClient client;
		private PunishDbContext database;
		private ILogger logger;
		private MessageBuilder builder;

		public RoleService(
			DiscordSocketClient _client,
			PunishDbContext _database,
			ILogger<PunishmentService> _logger,
			MessageBuilder _builder)
		{
			client = _client;
			database = _database;
			logger = _logger;
			builder = _builder;
		}

		public async Task DeleteRole(IGuildUser responsible, string roleName)
		{
			var guildConfig = database.Configurations.FirstOrDefault(x => x.GuildId == responsible.Guild.Id);
			if (guildConfig == null)
				throw new InvalidOperationException("The guild must first be configured before configuring a role");

			var roleConfig = database.Roles.FirstOrDefault(x => x.Guild.GuildId == responsible.Guild.Id && x.Name.StartsWith(roleName));
			if (roleConfig == null)
				throw new InvalidOperationException($"The role `{roleName}` does not exist or has not been configured correctly.");

			database.Roles.Remove(roleConfig);
			await database.SaveChangesAsync();

			var guild = await client.GetGuildAsync(roleConfig.Guild.GuildId);
			var channel = await guild.GetChannelAsync(guildConfig.LogChannelId) as ITextChannel;

			await channel.SendMessageAsync(builder.BuildRoleDeleteMessage(responsible, roleConfig));
		}

		public async Task CreateOrModifyRole(IGuildUser responsible, string roleName, Action<RoleConfiguration> configureRole)
		{
			var guildConfig = database.Configurations.FirstOrDefault(x => x.GuildId == responsible.Guild.Id);

			if (guildConfig == null)
				throw new InvalidOperationException("The guild must first be configured before configuring a role");

			var roleConfig = database.Roles.FirstOrDefault(x => x.Guild.GuildId == responsible.Guild.Id && x.Name.StartsWith(roleName));

			var added = false;
			if (roleConfig == null)
			{
				added = true;
				roleConfig = new RoleConfiguration{
					Guild = guildConfig,
					Name = roleName,
					Publicity = AutorolePublicity.Private
				};
			}

			configureRole(roleConfig);

			var role = responsible.Guild.GetRole(roleConfig.RoleId);
			if (responsible.Roles.Max(x => x.Position) <= role.Position)
				throw new InvalidOperationException("Roles must be more restrictive than yours.");

			if (added)
				database.Roles.Add(roleConfig);

			await database.SaveChangesAsync();

			var guild = await client.GetGuildAsync(roleConfig.Guild.GuildId);
			var channel = await guild.GetChannelAsync(guildConfig.LogChannelId) as ITextChannel;

			await channel.SendMessageAsync(builder.BuildRoleConfigureMessage(responsible, roleConfig, role));
		}

		public async Task AddRole(IGuildUser target, string roleName, IGuildUser responsible)
		{
			var guildConfig = database.Configurations.FirstOrDefault(x => x.GuildId == responsible.Guild.Id);
			if (guildConfig == null)
				throw new InvalidOperationException("The guild must first be configured before configuring a role");

			var roleConfig = database.Roles.FirstOrDefault(x => x.Guild.GuildId == target.Guild.Id && x.Name.StartsWith(roleName));
			if (roleConfig == null)
				throw new InvalidOperationException($"The role `{roleName}` does not exist or has not been configured correctly.");

			var role = target.Guild.GetRole(roleConfig.RoleId);

			if (roleConfig.Publicity == AutorolePublicity.Private && responsible.Roles.Max(x => x.Position) <= role.Position)
				throw new InvalidOperationException("You cannot add roles which are equal to or above yours!");

			await target.AddRolesAsync(role);

			var guild = await client.GetGuildAsync(roleConfig.Guild.GuildId);
			var channel = await guild.GetChannelAsync(guildConfig.LogChannelId) as ITextChannel;

			await channel.SendMessageAsync(builder.BuildRoleModifyMessage(true, target, responsible, role));
		}

		public async Task RemoveRole(IGuildUser target, string roleName, IGuildUser responsible)
		{
			var guildConfig = database.Configurations.FirstOrDefault(x => x.GuildId == responsible.Guild.Id);
			if (guildConfig == null)
				throw new InvalidOperationException("The guild must first be configured before configuring a role");

			var roleConfig = database.Roles.FirstOrDefault(x => x.Name.StartsWith(roleName));
			if (roleConfig == null)
				throw new InvalidOperationException($"The role `{roleName}` does not exist.");

			var role = target.Guild.GetRole(roleConfig.RoleId);

			if (roleConfig.Publicity == AutorolePublicity.Private && responsible.Roles.Max(x => x.Position) <= role.Position)
				throw new InvalidOperationException("You cannot remove roles which are equal to or above yours!");

			await target.RemoveRolesAsync(role);

			var guild = await client.GetGuildAsync(roleConfig.Guild.GuildId);
			var channel = await guild.GetChannelAsync(guildConfig.LogChannelId) as ITextChannel;

			await channel.SendMessageAsync(builder.BuildRoleModifyMessage(false, target, responsible, role));
		}

		public async Task ToggleRole(IGuildUser target, string roleName, IGuildUser responsible)
		{
			var roleConfig = database.Roles.FirstOrDefault(x => x.Name.StartsWith(roleName));
			if (roleConfig == null)
				throw new InvalidOperationException($"The role `{roleName}` does not exist.");

			var role = target.Guild.GetRole(roleConfig.RoleId);

			if (target.Roles.Any(x => x.Id == role.Id))
				await RemoveRole(target, roleName, responsible);
			else
				await AddRole(target, roleName, responsible);
		}
	}
}