using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Discord;
using Discord.WebSocket;

namespace Modatron.Services
{
	public class MessageDeleter
	{
		private ISelfUser currentUser;

		public MessageDeleter(DiscordSocketClient client)
		{
			currentUser = client.GetCurrentUser();
		}
		public Task DeleteMessages(params IUserMessage[] messages) => DeleteMessages(messages.AsEnumerable());

		public async Task DeleteMessages(IEnumerable<IUserMessage> messages)
		{
			if (messages.FirstOrDefault() == null)
				throw new InvalidOperationException("Tried to delete 0 messages");

			if (messages.Select(x => x.Channel).Distinct().Count() != 1)
				throw new InvalidOperationException("Tried to delete messages from multiple channels");
			
			var firstMessage = messages.FirstOrDefault();

			if (firstMessage.Channel is IGuildChannel)
			{
				// only delete the messages if we have permission

				var channel = firstMessage.Channel as IGuildChannel;
				var currentGuildUser = channel.Guild.GetCurrentUser();
				var perms = currentGuildUser.GetPermissions(channel);
				if (perms.ManageMessages || currentGuildUser.GuildPermissions.ManageMessages || currentGuildUser.GuildPermissions.Administrator)
				{
					await firstMessage.Channel.DeleteMessagesAsync(messages);
				}
			}
			else if (firstMessage.Channel is IDMChannel)
			{
				// we can always bulk delete in DM channels

				var channel = firstMessage.Channel as IDMChannel;
				await channel.DeleteMessagesAsync(messages);
			}
		}
	} 
}