using System;
using System.Linq;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;

namespace Modatron.Preconditions
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
	class RequireRoleAttribute : PreconditionAttribute
	{
		public string RoleName { get; }
		public StringComparison Comparison { get; }

		public RequireRoleAttribute(string roleName)
			: this(roleName, StringComparison.Ordinal)
		{}

		public RequireRoleAttribute(string roleName, StringComparison comparer)
		{
			RoleName = roleName;
			Comparison = comparer;
		}

		public override Task<PreconditionResult> CheckPermissions(IUserMessage context, Command executingCommand, object moduleInstance)
		{
			var author = (context.Author as IGuildUser);
			if (author != null)
			{
				var roles = author.Roles;
				if (roles.Any(x => x.Name.Equals(RoleName, Comparison)))
					return Task.FromResult(PreconditionResult.FromSuccess());
				else
					return Task.FromResult(PreconditionResult.FromError($"You require the `{RoleName}` role to do this!"));
			}
			else
			{
				return Task.FromResult(PreconditionResult.FromError("Command must be used in a guild!"));
			}
		}
	}
}