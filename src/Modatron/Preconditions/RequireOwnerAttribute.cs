using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;

namespace Modatron.Preconditions
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
	class RequireOwnerAttribute : PreconditionAttribute
	{
		public override Task<PreconditionResult> CheckPermissions(IUserMessage context, Command executingCommand, object moduleInstance)
		{
			// TODO: get a reference to the client and do this using application info
			if (context.Author.Id == 81062087257755648)
				return Task.FromResult(PreconditionResult.FromSuccess());
			else
				return Task.FromResult(PreconditionResult.FromError("You need to be the bot owner to do this!"));
		}
	}
}