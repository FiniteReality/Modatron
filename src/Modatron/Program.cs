﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;

namespace Modatron
{
	public class Program
	{
		public static void Main(string[] args)
		{
			var services = new ServiceCollection();
			
			
			services.AddLogging();

			var bot = new ModatronBot();
			bot.ConfigureServices(services);
			services.AddSingleton(bot);

			var provider = services.BuildServiceProvider();

			var factory = provider.GetRequiredService<ILoggerFactory>();

#if DEBUG
			factory.AddConsole(LogLevel.Trace, true)
				.AddDebug(LogLevel.Debug);
#else
			factory.AddConsole();
#endif

			bot.Run(provider).GetAwaiter().GetResult();
		}
	}
}
