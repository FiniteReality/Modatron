using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Modatron.Models
{
	public class PunishDbContext : DbContext
	{
		public DbSet<TimedPunishment> TimedPunishments { get; set; }
		public DbSet<Punishment> Punishments { get; set; }

		public DbSet<GuildConfiguration> Configurations { get; set; }

		public DbSet<RoleConfiguration> Roles { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			// Things I've learned through writing this code:
			// - EntityFramework does not like Configuration by default
			// - I have no idea how to get Configuration to work correctly
			// - ASP.NET Core does things really nicely

			var builder = new ConfigurationBuilder()
				.SetBasePath(System.IO.Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json");

			var connStr = builder.Build().GetConnectionString("main");

			optionsBuilder.UseMySql(connStr);
		}
	}
}