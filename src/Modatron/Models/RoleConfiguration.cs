using System.ComponentModel.DataAnnotations.Schema;

namespace Modatron.Models
{
	public enum AutorolePublicity
	{
		Public,
		Private
	};

	public class RoleConfiguration
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public GuildConfiguration Guild { get; set; }
		[Column(TypeName = "BIGINT UNSIGNED")]
		public ulong RoleId { get; set; }

		public AutorolePublicity Publicity { get; set; }
	}
}