using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Modatron.Models
{
	public enum PunishmentType
	{
		Warning,
		Softban,
		Ban
	}

	public class GuildConfiguration
	{
		public int Id { get; set; }
		[Column(TypeName = "BIGINT UNSIGNED")]
		public ulong GuildId { get; set; }
		[Column(TypeName = "BIGINT UNSIGNED")]
		public ulong LogChannelId { get; set; }
	}

	public class Punishment
	{
		public int Id { get; set; }

		public GuildConfiguration Guild { get; set; }
		[Column(TypeName = "BIGINT UNSIGNED")]
		public ulong MessageId { get; set; }
		[Column(TypeName = "BIGINT UNSIGNED")]
		public ulong ChannelId { get; set; }
		public DateTime Time { get; set; } = DateTime.UtcNow;

		public PunishmentType Type { get; set; }

		[Column(TypeName = "BIGINT UNSIGNED")]
		public ulong TargetUser { get; set; }
		[Column(TypeName = "BIGINT UNSIGNED")]
		public ulong ResponsibleUser { get; set; }

		public string Reason { get; set; }
	}

	public class TimedPunishment : Punishment
	{
		public TimeSpan Duration { get; set; }
	}
}